// Config Flags
var attack_mode = true;
var move_attack = true;
var draw_ranges = true;

//Global Vars
var mapTiles = pathMap(G.maps[character.map].data);

if (draw_ranges) {
	setInterval(function () {
		clear_drawings();
		drawRange();
	}, 100);
}

setInterval(function () {
	usehp();
	loot();
	staticAttack();
}, 1000 / 10); // Loops every 1/4 seconds.

function staticAttack() {

	if (!attack_mode || character.moving)
		return;

	var target = get_targeted_monster();
	if (target && target.target && target.target != character.name) {
		change_target(null, true);
		return;
	}

	if (!target) {
		target = getNewMonster({
				min_xp : 100,
				max_att : 120,
				no_target : true
			});
		if (target) {
			if (target.target && target.target != character.name) {
				return;
			}
			change_target(target);
		} else {
			set_message("No Monsters");
			return;
		}
	}

	if (!in_attack_range(target)) {

		if (move_attack) {
			move(
				character.real_x + (target.real_x - character.real_x) / 6,
				character.real_y + (target.real_y - character.real_y) / 6);
		} else {
			change_target(null, true);
		}
	} else if (can_attack(target)) {
		set_message("Attacking");
		attack(target);
	}
}

function usehp() {
	if (safeties && mssince(last_potion) < 600)
		return;
	var used = false;
	if (new Date() < parent.next_potion)
		return;
	if (character.mp / character.max_mp < 0.2)
		parent.use('mp'), used = true;
	else if (character.hp / character.max_hp < 0.2)
		parent.use('hp'), used = true;
	else if (character.mp / character.max_mp < 0.3)
		parent.use('mp'), used = true;
	else if (character.hp < (character.max_hp - 200))
		parent.use('hp'), used = true;
	else if (character.mp < (character.max_mp - 200))
		parent.use('mp'), used = true;
	if (used)
		last_potion = new Date();

}
function drawRange(arg) {

	var range;
	var color = 0x00F33E;
	for (ent in parent.entities) {
		let arg = parent.entities[ent];
		if (arg.type == 'monster') {
			range = G.monsters[arg.mtype].range;
			color = 0x0000ff;
		} else if (arg.type == 'character' && arg.name != character.name) {
			range = arg.range;
			color = 0xff0000;
		}
		draw_circle(arg.real_x, arg.real_y, range, 1, color);
	}
	draw_circle(character.real_x, character.real_y, range, 1, 0x00F33E);
	
}
function getNewMonster(args) {
	var min_d = 400,
	target = null;
	for (id in parent.entities) {
		var current = parent.entities[id];
		if (current.mtype == "pinkgoo") {
			return current;
		}
	}
	for (id in parent.entities) {
		var current = parent.entities[id];
		if (current.type != "monster" || args.min_xp && current.xp < args.min_xp || args.max_att && current.attack > args.max_att || current.dead || (current.target && current.target != character.name))
			continue;
		if (args.no_target && current.target && current.target != character.name)
			continue;
		var c_dist = parent.distance(character, current);
		if (c_dist < min_d)
			min_d = c_dist, target = current;
	}
	return target;
}
function teleport(dest) {
	parent.socket.emit('transport', {
		to : teleDictionary[dest].to,
		s : teleDictionary[dest].s
	});
}

function pathMap(currMap) {
	const GRIDSIZE = 16;
	class Box {
		constructor(x1, y1, x2, y2) {
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
		}

		intersects(box) {
			return (this.x1 <= box.x2 &&
				box.x1 <= this.x2 &&
				this.y1 <= box.y2 &&
				box.y1 <= this.y2);
		}

		drawBox() {
			draw_circle(this.x1, this.y1, 2, 1, 0xFF0000);
			draw_circle(this.x2, this.y2, 2, 1, 0xFF0000);
			draw_line(this.x1, this.y1, this.x1, this.y2, 2, 0xFFFFFF);
			draw_line(this.x1, this.y2, this.x2, this.y2, 2, 0xFFFFFF);
			draw_line(this.x2, this.y2, this.x2, this.y1, 2, 0xFFFFFF);
			draw_line(this.x2, this.y1, this.x1, this.y1, 2, 0xFFFFFF);
		}
	}
	class MapGrid {
		constructor(m) {
			this.map = m;
			this.obstacles = [];
			this.tiles = [];
		}
		mapInit() {
			for (let line of this.map.x_lines) {
				this.obstacles.push(new Box(
						line[0] - 3,
						line[1] - 3,
						line[0] + 3,
						line[2] + 7));
			}

			for (let line of this.map.y_lines) {
				this.obstacles.push(new Box(
						line[1] - 3,
						line[0] - 3,
						line[2] + 3,
						line[0] + 7));
			}
		}
		drawGrid() {
			for (let ob of this.obstacles) {
				ob.drawBox();
			}
		}
		createMapArray() {
			for (let y = this.map.min_y; y < this.map.max_y; y = y + 16) {
				let xlines = [];
				for (let x = this.map.min_x; x < this.map.max_x; x = x + 16) {
					let gridIntersect = false;
					for (let ob of this.obstacles) {
						if (ob.intersects(new Box(x, y, x + GRIDSIZE, y + GRIDSIZE))) {
							gridIntersect = true;
							xlines.push(1);
							break;
						}
					}
					if (!gridIntersect) {
						xlines.push(0);
					}
				}
				this.tiles.push(xlines);
			}
			return this.tiles;
		}

	}
	let map = new MapGrid(currMap);
	map.mapInit();
	let grid = map.createMapArray();

	//map.drawGrid();

	return grid;
}
/*setInterval(function () {
clear_drawings();
let map = G.maps[character.map].data;

let min_x = map.min_x;
let min_y = map.min_y;
for(let y = 0;y<mapTiles.length;y++){
for(let x = 0;x<mapTiles[0].length;x++){
if(mapTiles[y][x]){
draw_circle(min_x+(x*16)+8,min_y+(y*16)+8,8,1,0xFF0000);
}else{
draw_circle(min_x+(x*16)+8,min_y+(y*16)+8,8,1,0x00FF00);
}
}
}
}, 10000);*/
